import React, {Component} from 'react';
import {View,StyleSheet,ScrollView,Image,} from 'react-native';

class FlexBox extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.lady1}>
                    <Image source={require('../aset/a.jpg')} style={{width:150,height:50}}/>
                    <Image source={require('../aset/a1.jpg')} style={{width:70,height:30,marginLeft:140}}/>
                    <Image source={require('../aset/a2.jpg')} style={{width:50,height:30,}}/>
                </View>
                <View style={styles.ladi2}>
                    <ScrollView>
                        <ScrollView horizontal>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        <Image source={require('../aset/a3.jpg')} style={{width:60,height:60,borderRadius:30,borderWidth:2,borderColor:'red',marginLeft:10}}/>
                        </ScrollView>
                        <Image source={require('../aset/a4.jpg')} style={{width:425,height:590}}/>
                        <Image source={require('../aset/a4.jpg')} style={{width:435,height:590}}/>
                        <Image source={require('../aset/a4.jpg')} style={{width:435,height:590}}/>
                        <Image source={require('../aset/a4.jpg')} style={{width:435,height:590}}/>
                        <Image source={require('../aset/a4.jpg')} style={{width:435,height:590}}/>
                    </ScrollView>
                </View>
                <View style={styles.ladi3}>
                    <Image source={require('../aset/a5.jpg')} style={{width:40,height:40}}/>
                    <Image source={require('../aset/a6.jpg')} style={{width:40,height:40}}/>
                    <Image source={require('../aset/a7.jpg')} style={{width:50,height:50}}/>
                    <Image source={require('../aset/a8.jpg')} style={{width:50,height:50}}/>
                    <Image source={require('../aset/a9.jpg')} style={{width:50,height:50}}/>
                </View>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1
    },
    lady1:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'row'
    },
    ladi2:{
        flex:9,
        backgroundColor:'white'
    },
    ladi3:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'row',
        justifyContent:'space-around'
    },
});
export default FlexBox;